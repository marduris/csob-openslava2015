<%-- (3) - define our own gsp view --%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
    	<%-- (3) - define our own gsp view --%>
        <!-- Custom CSS -->
        <asset:stylesheet src="csob/dashboard.css"/>

        <!-- (8) angularjs intro + basic intro -->
        <asset:javascript src="csob/angular-app.js"/>

        <!-- (9) angularjs, templates examples -->
        <asset:javascript src="csob/open-directive.js"/>

        <!-- (9) angularjs, templates examples -->
        <asset:javascript src="csob/open-controller.js"/>
    </head>

    <body>
    	<%-- (3) - define our own gsp view --%>
        <h1>Dashboard</h1>

		<%-- (3) - define our own gsp view --%>
        <div id="dashboard">
            <p>List of events ...</p>
            <ul>
                <li>Something awesome just happen !</li>
                <li>Well, not so awesome but still worth of see</li>
                <li>-------</li>
	
				<%-- (6) - fetch list of Events and show it --%>
                <g:each in="${events}" var="event">
                    <li>${event.getCreated()} | ${event.getMessage()} (${event.getUuid()})</li>
                </g:each>
            </ul>
        </div>

        <!-- (8) angularjs intro + basic intro -->
        <div ng-app="openslava">
            <input type="text" ng-model="hi" />
            <p>'{{hi}}'</p>

            <div ng-openslava></div>

            <!-- (11) angularjs resource to get list of events -->
            <div ng-controller="opencontroller">
                <p>{{whoami}}</p>

                <span ng-click="refreshData()">refresh</span>
                <div ng-repeat="event in events">
                    <p>{{event.created}} | {{event.message}} | {{event.uuid}}</p>
                </div>
            </div>

        </div>
    </body>
</html>