%{-- (9) angularjs, templates examples --}%
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <body>
        Hi, in GSP #2
        <g:each in="${events}" var="event">
            <li>${event.getCreated()} | ${event.getMessage()} (${event.getUuid()})</li>
        </g:each>
    </body>
</html>