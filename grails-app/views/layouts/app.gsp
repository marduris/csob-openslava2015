<%--
  (2) - our own layouts
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>ČSOB@Openslava2015</title>

        <!-- ! Grails assets plugins -->
        <asset:link rel="shortcut icon" href="csob/favicon.ico" type="image/x-icon"/>

        <!-- (8) angularjs intro + basic intro -->
        <!-- must be before 'layoutHead' otherwise angular will not be loaded before app module -->
        <asset:javascript src="csob/angular.min.js"/>

        <!-- (11) angularjs resource to get list of events -->
        <asset:javascript src="csob/angular-resource.min.js"/>

        <!-- ! Print collected items for head -->
        <g:layoutHead/>
    </head>

    <body>
        <!-- ! Print content -->
        <g:layoutBody/>
        
        <!-- (14) integrate spring security -->
    	<g:link controller='logout'>Logout</g:link>
    </body>
</html>