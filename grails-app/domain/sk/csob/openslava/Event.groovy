package sk.csob.openslava

import grails.rest.Resource

/**
 * (10) grails rest domain api
 */
@Resource
/**
 * (4) - Event Domain Class
 */
class Event {

    String message

    Date created

    String uuid

}
