import sk.app.openslava.security.SecurityRole
import sk.app.openslava.security.User
import sk.app.openslava.security.UserSecurityRole;

class BootStrap {
 
    def init = { servletContext ->
		// (14) integrate spring security
		SecurityRole userRole = new SecurityRole("ROLE_ADMIN")
		userRole.save()

		User testUser = new User("test", "test")
		testUser.save()

		UserSecurityRole.create(testUser, userRole)
    }
    def destroy = {
    }
}
