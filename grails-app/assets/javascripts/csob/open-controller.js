// (11) angularjs resource to get list of events
angularapp.controller('opencontroller', ['$scope', '$resource', function($scope, $resource) {
    $scope.whoami = 'ČSOB@Openslava';

    var Event = $resource("/events")

    $scope.refreshData = function(){
        $scope.events = Event.query(function() {
            console.log($scope.events);
        })
    }

}]);