// (9) angularjs, templates examples
angularapp.directive('ngOpenslava', function() {
    return {
        restrict: 'A',
        transclude: true,
        scope: {

        },
        link: function(scope, element, attrs) {

        },
        //template:
            // inside template definition
            //'<div>Hi</div>',

        templateUrl:
            // /assets/
            //'/assets/csob/ng-openslava.html',
            // UrlMappings to .gsp views
            //'/ng-openslava',
            // dedicated controller with gsp views
            '/welcome/angular',
        replace: true
    };
})