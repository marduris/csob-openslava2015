package sk.csob.openslava.api.rest

import grails.rest.RestfulController
import sk.csob.openslava.Event

/**
 * (10) grails rest domain api
 */
class EventController extends RestfulController {

    static responseFormats = ['json']

    EventController(){
        super(Event)
    }
}
