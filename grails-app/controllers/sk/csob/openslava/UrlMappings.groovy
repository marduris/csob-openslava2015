package sk.csob.openslava
class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')

        /* (9) angularjs, templates examples */
        "/ng-openslava"(view:"/angularjs/ng-openslava")

        /* (10) grails rest domain api */
        "/events"(resources: "Event")
    }
}
