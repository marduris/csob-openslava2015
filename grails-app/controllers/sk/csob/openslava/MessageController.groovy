package sk.csob.openslava

import grails.plugin.springsecurity.annotation.Secured

// (14) integrate spring security
@Secured(["permitAll()"])
/**
 * (5) controller with put gsp
 */
class MessageController {

    /**
     * Create new event
     * (5) controller with put gsp
     */
    def index = {
        render(view: "create", layout: "app")
    }

    /**
     * Put new event
     * (5) controller with put gsp
     */
    def put = {
        Event event = new Event(params)
        event.setCreated(new Date())
        event.setUuid(generateIdentifier())

        if(event.validate()){
            event.save()
        } else {
            render "error"
        }

        render "saved"
    }

    /**
     * Generate new identifier
     * (5) controller with put gsp
     *
     * @return
     */
    def generateIdentifier(){
        return UUID.randomUUID().toString()
    }
}