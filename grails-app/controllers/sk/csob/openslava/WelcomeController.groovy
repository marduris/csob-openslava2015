package sk.csob.openslava

import grails.plugin.springsecurity.annotation.Secured

// (14) integrate spring security
@Secured(['ROLE_ADMIN'])
/**
 * Created by martin on 10/2/15.
 */
class WelcomeController {

    /**
     * (1) - welcome screen
     */
    def index = {
        render "welcome screen"
    }

    /**
     * (2) - layouts with favicon
     */	
    def dashboard = {
		// (2) - layouts with favicon
        //render(view: "dashboard", layout: "app")

        // (6) - fetch list of Events and show it
        render(view: "dashboard", layout: "app", model: [events: Event.findAll()])
    }

    /**
     * (9) angularjs, templates examples
     */
    def angular = {
        render(view: "angular-dashboard", layout: "angular-layout", model: [events: Event.findAll()])
    }
}
