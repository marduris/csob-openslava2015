package sk.csob.openslava

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * (7) unit test of controller, run with gradle
 */
@TestFor(MessageController)
class MessageControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect:"fix me"
            true == true
    }

    void "is UUID really random"(){
        def uuid1 = controller.generateIdentifier()
        def uuid2 = controller.generateIdentifier()

        expect: "should be different"
            uuid1 != uuid2
    }
}